// Upgrade NOTE: now shader work on mobile
// Upgrade NOTE: now shader have 2 lods, just disable reflection in inspector to see first lod

Shader "Mobile/OceanOriginal" 
{
	Properties 
	{
		_BumpAmt ("Distortion", Range (0, 128)) = 10
		_BumpAmt ("Distortion", int) = 10
		_InputFactor ("Input settings(clamp min,max / lerp max,min",Vector) = (0,1,0,1)
	    _OutputFactor ("Output settings(clamp min,max / lerp max,min",Vector) = (0,1,0,1)
	    _Transparencyfactor ("Tweaks: opacity effect on transparancy, maximum opacity, minimum effect, maximum effect",Vector) = (0,1,0,1)

	    _SurfaceColor ("SurfaceColor", Color) = (1,1,1,1)
	    _WaterColor ("WaterColor", Color) = (1,1,1,1)
		_Refraction ("Refraction (RGB)", 2D) = "white" {}
		_Reflection ("Reflection (RGB)", 2D) = "white" {}
		_Fresnel ("Fresnel (A) ", 2D) = "gray" {}
		_Bump ("Bump (RGB)", 2D) = "bump" {}
		_Foam ("Foam (RGB)", 2D) = "white" {}
		_Size ("Size", Vector) = (1, 1, 1, 1)
		_SunDir ("SunDir", Vector) = (0.3, -0.6, -1, 0)
		_SpecColor ("SunColor", Color) = (0,0,0,1)
		
		_SurfaceColorLod1 ("SurfaceColor LOD1", Color) = (1,1,1,0.5)
		_WaterColorLod1 ("WaterColor LOD1", Color) = (1,1,1,0.5)
		_WaterTex ("Water LOD1 (RGB)", 2D) = "white" {}
		
		_Alpha ("Alpha", Range (0.01, 1)) = 0.9
		_FoamStrength ("FoamStrength", Range (0, 1)) = 0.9
	}
SubShader {
		Blend SrcAlpha OneMinusSrcAlpha
		Tags { "Queue"="Transparent" "RenderType"="Transparent" "LightMode" = "Always" }
		LOD 100
		
		GrabPass {
			Name "BASE"
			Tags { "LightMode" = "Always" }
		}
				
    	Pass {
    		Name "BASE"
			Cull Off ZWrite On
			Fog {Mode off}
			CGPROGRAM
			#pragma target 5.0
			#pragma exclude_renderers xbox360
			#pragma vertex vert Lambert nolightmap
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			struct appdata_t {
			    float4 vertex : POSITION;
			    float2 texcoord: TEXCOORD0;
			};

			struct v2f 
			{
    			float4 pos : SV_POSITION;
    			float4  projTexCoord : TEXCOORD0;
    			float2  bumpTexCoord : TEXCOORD1;
    			float3  viewDir : TEXCOORD2;
    			float3  objSpaceNormal : TEXCOORD3;
    			float3  lightDir : TEXCOORD4;
    			float2  foamStrengthAndDistance : TEXCOORD5;
    			float4 uvgrab : TEXCOORD6;
			    float2 uvbump : TEXCOORD7;
			    float2 uvmain : TEXCOORD8;
			};

			float4 _Size;
			float4 _SunDir;
			float4 _Bump_ST;
			float4 _WaterTex_ST;
			float _BumpAmt;

			v2f vert (appdata_tan v)
			{
				v2f o;
				//UNITY_INITIALIZE_OUTPUT(v2f,o);
    
    			o.bumpTexCoord.xy = v.vertex.xz/float2(_Size.x, _Size.z)*5;
    
    			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
    
    			o.foamStrengthAndDistance.x = v.tangent.w;
    			o.foamStrengthAndDistance.y = clamp(o.pos.z, 0, 1.0);
    
    
  				float4 projSource = float4(v.vertex.x, 0.0, v.vertex.z, 1.0);
    			float4 tmpProj = mul( UNITY_MATRIX_MVP, projSource);
    			o.projTexCoord = tmpProj;

    			float3 objSpaceViewDir = ObjSpaceViewDir(v.vertex);
    			float3 binormal = cross( normalize(v.normal), normalize(v.tangent.xyz) );
				float3x3 rotation = float3x3( v.tangent.xyz, binormal, v.normal );
    
    			o.objSpaceNormal = v.normal;
    			o.viewDir = mul(rotation, objSpaceViewDir);
    
    			o.lightDir = mul(rotation, float3(_SunDir.xyz));
                
                //refraction
    			#if UNITY_UV_STARTS_AT_TOP
   				float scale = -1.0;
			    #else
			    float scale = 1.0;
			    #endif
			    o.uvgrab.xy = (float2(o.pos.x, o.pos.y*scale) + o.pos.w) * 0.5;
			    o.uvgrab.zw = o.pos.zw;
			    o.uvbump = TRANSFORM_TEX( v.texcoord, _Bump );
			    o.uvmain = TRANSFORM_TEX( v.texcoord, _WaterTex );	
			    
			    //get the texture distance
			    
    			return o;
			}
			
			sampler2D _Refraction;
			sampler2D _Reflection;
			sampler2D _Fresnel;
			sampler2D _Bump;
			sampler2D _Foam;
			half4 _SurfaceColor;
			half4 _WaterColor;
			float4 _SpecColor;
			fixed _Alpha;
			half _FoamStrength;
			sampler2D _GrabTexture;
			float4 _GrabTexture_TexelSize;
			sampler2D _WaterTex;
			float4 _InputFactor;
			float4 _OutputFactor;
			float4 _Transparencyfactor;
			
			half4 frag (v2f i) : COLOR
			{
				half3 normViewDir = normalize(i.viewDir);

				half4 buv = half4(i.bumpTexCoord.x + _Time.x * 0.003, i.bumpTexCoord.y + _SinTime.x * 0.02, i.bumpTexCoord.x + _Time.y * 0.004, i.bumpTexCoord.y + _SinTime.y * 0.05);
				//half4 buv = half4(i.bumpTexCoord.x + _Time.x, i.bumpTexCoord.y, i.bumpTexCoord.x + _Time.x, i.bumpTexCoord.y);
				
				half3 tangentNormal0 = (tex2D(_Bump, buv.xy).rgb * 2.0) - 1;
				half3 tangentNormal1 = (tex2D(_Bump, buv.zw).rgb * 2.0) - 1;
				half3 tangentNormal = normalize(tangentNormal0 + tangentNormal1);
				
				float2 projTexCoord = 0.5 * i.projTexCoord.xy * float2(1, _ProjectionParams.x) / i.projTexCoord.w + float2(0.5, 0.5);
	
				// refraction				
				half2 bump = UnpackNormal(tex2D(_Bump, i.uvbump)).rg;
				float refractOffset = bump * _BumpAmt * _GrabTexture_TexelSize.xy;
				//i.uvgrab.xy = refractoffset * i.uvgrab.z + i.uvgrab.xy;
				i.uvgrab.xy = refractOffset * i.uvgrab.z + i.uvgrab.xy;
				half4 col = tex2Dproj( _GrabTexture, UNITY_PROJ_COORD(i.uvgrab));
			    half4 tint = tex2D( _WaterTex, i.uvmain );
			   
			    half4 input = lerp(_InputFactor.z,_InputFactor.w,clamp(1-tint.a,_InputFactor.x,_InputFactor.y));
			    half4 output = lerp(_OutputFactor.z,_OutputFactor.w,clamp(1-input.a,_OutputFactor.x,_OutputFactor.y));
			    half4 effector = lerp(_Transparencyfactor.w,_Transparencyfactor.z,1-tint.a*_Transparencyfactor.x);
			    //half4 effector = _Transparencyfactor.y*(clamp(lerp(_Transparencyfactor.w,_Transparencyfactor.z,1-tint.a),0,1));
			    //half4 trans = col*tint*effector-lerp(_Transparencyfactor.w,_Transparencyfactor.z,1-tint.a);
			    //half4 trans = col*tint* clamp(lerp(_Transparencyfactor.w,_Transparencyfactor.z,tint.a)*_Transparencyfactor.x,1-_Transparencyfactor.y,1);
			    //half4 trans = col*tint*tint.a;
			    //half4 trans = (col*tint)-(1-effector.a)*_Transparencyfactor.x;
			    half4 trans = 1;
			    trans.rgb = clamp((tint.rgb*col.rgb)-(effector/_Transparencyfactor.y*_Transparencyfactor.y),0,1);
			    //trans = trans-(1-(tint.a*_Transparencyfactor.y))*_Transparencyfactor.x;
			    //return trans+output.rgba*_Transparencyfactor.y;
			    //return trans+output.rgba*effector;
			    float4 refractionOut = trans+output.rgba*effector*(2*(1/_Transparencyfactor.x)+trans);
	
	
				half4 result = half4(0, 0, 0, 1);
	
				float2 bumpSampleOffset = i.objSpaceNormal.xz * 0.05 + tangentNormal.xy * 0.05;
	
				half3 reflection = tex2D(_Reflection, projTexCoord.xy + bumpSampleOffset).rgb * _SurfaceColor.rgb;
				half3 refraction = tex2D(_Refraction, projTexCoord.xy + bumpSampleOffset).rgb * _WaterColor.rgb;

				float fresnelLookup = dot(tangentNormal, normViewDir);
	
				float bias = 0.06;
				float power = 4.0;
				float fresnelTerm = bias + (1.0-bias)*pow(1.0 - fresnelLookup, power);
	
				float foamStrength = i.foamStrengthAndDistance.x / 2;
				
				//figure out how to move from one foam level to another
				//float4 foamTex = tex2D(_Foam, 0);

				half4 foam = clamp(tex2D(_Foam, i.bumpTexCoord.xy * 1.0) - 0.25, 0.0, 1.0) * foamStrength;
				half4 foam2 = clamp(tex2D(_Foam,i.bumpTexCoord.xy * .5) - 0.9, 0.0, 1.0) * foamStrength;
				//foam = half4(foam.r,0,foam2.b,0);
				
				float foamOut = lerp(foam.r, foam.b, _FoamStrength);

				float3 halfVec = normalize(normViewDir - normalize(i.lightDir));
    			float specular = pow(max(dot(halfVec, tangentNormal.xyz), 0.0), 250.0);

				//result.rgb = lerp(refraction, reflection, fresnelTerm) + clamp(foamOut, 0.0, 0.5) + specular * _SpecColor;
				result.rgb = lerp(refraction, reflection, fresnelTerm) + clamp(foamOut, 0.0, 0.5) + specular * _SpecColor;
				
                //result.a = _Alpha * clamp(foamOut,0.98,1.0);
                result = result * refractionOut;
    			return result;
			}
			ENDCG
		}
    }	
    SubShader {
        LOD 4
    	Pass {

		    Blend One OneMinusSrcAlpha
			Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
			
			CGPROGRAM
			#pragma exclude_renderers xbox360
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f 
			{
    			float4 pos : SV_POSITION;
    			float2  bumpTexCoord : TEXCOORD1;
    			float3  viewDir : TEXCOORD2;
    			float3  objSpaceNormal : TEXCOORD3;
    			float3  lightDir : TEXCOORD4;
    			float2  foamStrengthAndDistance : TEXCOORD5;
			};

			float4 _Size;
			float4 _SunDir;
			sampler2D _WaterTex;
            
			v2f vert (appdata_tan v)
			{
    			v2f o;
    
    			o.bumpTexCoord.xy = v.vertex.xz/float2(_Size.x, _Size.z)*5;
    
    			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
    
    			o.foamStrengthAndDistance.x = v.tangent.w;
    			o.foamStrengthAndDistance.y = clamp(o.pos.z, 0, 1.0);
    
    
  				float4 projSource = float4(v.vertex.x, 0.0, v.vertex.z, 1.0);
    			float4 tmpProj = mul( UNITY_MATRIX_MVP, projSource);

    			float3 objSpaceViewDir = ObjSpaceViewDir(v.vertex);
    			float3 binormal = cross( normalize(v.normal), normalize(v.tangent.xyz) );
				float3x3 rotation = float3x3( v.tangent.xyz, binormal, v.normal );
    
    			o.objSpaceNormal = v.normal;
    			o.viewDir = mul(rotation, objSpaceViewDir);
    
    			o.lightDir = mul(rotation, float3(_SunDir.xyz));

    			return o;
			}

			sampler2D _Fresnel;
			sampler2D _Bump;
			
			sampler2D _Foam;
			half4 _SurfaceColorLod1;
			half4 _WaterColorLod1;
			fixed _Alpha;

			half4 frag (v2f i) : COLOR
			{
				half3 normViewDir = normalize(i.viewDir);

				half4 buv = half4(i.bumpTexCoord.x + _Time.x * 0.03, i.bumpTexCoord.y + _SinTime.x * 0.2, i.bumpTexCoord.x + _Time.y * 0.04, i.bumpTexCoord.y + _SinTime.y * 0.5);
                
                half2 buv2 = half2(i.bumpTexCoord.x - _Time.y * 0.05, i.bumpTexCoord.y);
                
				half3 tangentNormal0 = (tex2D(_Bump, buv.xy).rgb * 2.0) - 1;
				half3 tangentNormal1 = (tex2D(_Bump, buv.zw).rgb * 2.0) - 1;
				half3 tangentNormal = normalize(tangentNormal0 + tangentNormal1);

				half4 result = half4(0, 0, 0, 1);

	            half3 tex = tex2D(_WaterTex, buv2*2) * _WaterColorLod1 * 0.5;
                
				float fresnelLookup = dot(tangentNormal, normViewDir);
	
				float bias = 0.06;
				float power = 4.0;
				float fresnelTerm = bias + (1.0-bias)*pow(1.0 - fresnelLookup, power);
	
				float foamStrength = i.foamStrengthAndDistance.x * 1.8;
	
				half4 foam = clamp(tex2D(_Foam, i.bumpTexCoord.xy * 1.0)  - 0.5, 0.0, 1.0) * foamStrength;

				float3 halfVec = normalize(normViewDir - normalize(i.lightDir));
    			float specular = pow(max(dot(halfVec, tangentNormal.xyz), 0.0), 250.0);
    			
                
                
				result.rgb = lerp(_SurfaceColorLod1.rgb, tex.rgb, fresnelTerm) + clamp(foam.r, 0.0, 1.0) + specular;
                result.a = _Alpha;

    			return result;
			}
			ENDCG
		}
    }
}
